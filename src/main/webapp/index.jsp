<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<h2>Le cinema de Joe</h2>
<fieldset><legend align="center"><h2>Tri dans l'ordre des titres</h2></legend>
<p><a href='titrefilm?sort=note'>Trier les titres par notes, dans l'ordre croissant</a></p>
<p><a href='titrefilm?sort=notedesc'>Trier les titres par notes, dans l'ordre décroissant</a></p>
</fieldset>
<form method='post' role="search" action="recherche"><legend align="center"><h2>Recherche d'un titre</h2></legend>
    <div class="search-control">
        <input type="search" id="site-search" name="q" placeholder="Rechercher sur le site..." aria-label="Chercher dans le contenu du site">
        <button>Search</button>
    </div>
    <!-- Si la checkbox est cochée alors la tâche est envoyée en tant que tâche globale -->
    <label for="global">Enregistrer la Session</label><input type="checkbox" name="cbGlobal" id="global" value="Global" />
</form>
</body>
</html>

