package fr.laerce.cinema;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.sql.DatabaseMetaData;
import java.util.*;

/**
 * Created by fred on 03/02/2016.
 */
public class AfficheTitreFilm extends HttpServlet {
    List<String> todos = new ArrayList<String>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        List<String> todoSession = (List<String>) session.getAttribute("todos");
        if(todoSession == null){
            todoSession = new ArrayList<String>();
            session.setAttribute("todos", todoSession);
        }//////////////////////////////////////


        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Liste des films</title>");
        out.println("<meta charset='utf-8'/>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Liste des films</h1>");
        out.println("<ul>");

        //Boucle pour afficher les titres des films en lien qui renvoient vers leur description
        FilmsDonnees fd = new FilmsDonnees();
        String sort = request.getParameter("sort");
        if(sort == null) {
            //Afficher les films triés par ordre croissant
            out.println("<p>FILMS TRIÉS PAR NOM DANS L'ORDRE CROISSANT</p>");
            Collections.sort(fd.lesFilms, new FilmComparateur());
        } else if ("note".equals(sort)) {
            //Afficher les films triés par note dans l'ordre croissant
            out.println("<p>FILMS TRIÉS PAR NOTE DANS L'ORDRE CROISSANT</p>");
            Collections.sort(fd.lesFilms, new Comparator<Film>() {
                @Override
                public int compare(Film o1, Film o2) {
                    return Double.compare(o1.note, o2.note);
                }
            });
        } else if ("notedesc".equals(sort)) {
            //Afficher les films triés par ordre croissant
            out.println("<p>FILMS TRIÉS PAR NOTE DANS L'ORDRE DÉCROISSANT</p>");
            Collections.sort(fd.lesFilms, (o1, o2) -> Double.compare(o2.note, o1.note));
        }

        for (Film film : fd.lesFilms) {
            out.println("<a href='/affichedetailsfilm?id="+ film.id + "'><li>" + film.titre + "("+ film.note + ")<p><img width='100px' height='auto' src='affiche?id="+ film.id + "' /></p></li></a>");
        }/**/
        out.println("</ul>");

        out.println("");
        out.println("</body>");
        out.println("</html>");
    }
}
