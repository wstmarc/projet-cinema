package fr.laerce.cinema;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by fred on 03/02/2016.
 */
//@WebServlet(name = "Affiche")
public class AfficheDetailsFilm extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String recherche = request.getParameter("q");
        Integer id = Integer.parseInt(request.getParameter("id"));
        FilmsDonnees fd = new FilmsDonnees();
        Film film = fd.getById(id);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>#ID " + film.id + "#");
        out.println("</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<fieldset><legend align='center'><h2>"+ film.titre +"</h2></legend>");
        //out.println("<p>Nom: " + film.afficheNom + "</p>");
        out.println("<p align='center'><img src='affiche?id="+ film.id + "' />" + "</p>");
        out.println("<p align='center' >Note: " + film.note + " / 5</p>");
        out.println("Votre recherche: " + recherche);
        out.println("</fieldset>");
        out.println("</body>");
        out.println("</html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        FilmsDonnees fd = new FilmsDonnees();
        Film film = fd.getById(id);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>#ID " + film.id + "#");
        out.println("</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<fieldset><legend align='center'><h2>"+ film.titre +"</h2></legend>");
        //out.println("<p>Nom: " + film.afficheNom + "</p>");
        out.println("<p align='center'><img src='affiche?id="+ film.id + "' />" + "</p>");
        out.println("<p align='center' >Note: " + film.note + " / 5</p>");
        out.println("</fieldset>");
        out.println("</body>");
        out.println("</html>");
    }
}
