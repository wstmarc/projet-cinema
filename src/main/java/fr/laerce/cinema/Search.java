package fr.laerce.cinema;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.DatabaseMetaData;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by fred on 03/02/2016.
 */
public class Search extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FilmsDonnees fd = new FilmsDonnees();
        String recherche = request.getParameter("q");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Liste des films</title>");
        out.println("<meta charset='utf-8'/>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Liste des films</h1>");
        out.println("<ul>");

        out.println("<h1>Votre recherche: " + recherche + "</h1>");
        int nbfilmtrouves = 0;
        for (Film film : fd.lesFilms) {
            if(recherche.toLowerCase() != "" && (film.titre.toLowerCase().contains(recherche.toLowerCase()))) {
                out.println("<a href='/affichedetailsfilm?id=" + film.id + "'><li>" + film.titre + "(" + film.note + ")<p><img width='100px' height='auto' src='affiche?id=" + film.id + "' /></p></li></a>");
                nbfilmtrouves += 1;
            }
        }
        if(recherche == "") {
            out.println("Votre recherche est vide.");
        } else {
            if(nbfilmtrouves == 0) {
                out.println("Votre recherche ne correspond à aucun film.");
            }
        }

        out.println("</ul>");

        out.println("");
        out.println("</body>");
        out.println("</html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*FilmsDonnees fd = new FilmsDonnees();
        String recherche = request.getParameter("q");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Liste des films</title>");
        out.println("<meta charset='utf-8'/>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Liste des films</h1>");
        out.println("<ul>");

        out.println("<h1>Votre recherche: " + recherche + "</h1>");
        int nbfilmtrouves = 0;
        for (Film film : fd.lesFilms) {
            if(recherche.toLowerCase() != "" && (film.titre.toLowerCase().contains(recherche.toLowerCase()))) {
                out.println("<a href='/affichedetailsfilm?id=" + film.id + "'><li>" + film.titre + "(" + film.note + ")<p><img width='100px' height='auto' src='affiche?id=" + film.id + "' /></p></li></a>");
                nbfilmtrouves += 1;
            }
        }
        if(recherche == "") {
            out.println("Votre recherche est vide.");
        } else {
            if(nbfilmtrouves == 0) {
                out.println("Votre recherche ne correspond à aucun film.");
            }
        }

        out.println("</ul>");

        out.println("");
        out.println("</body>");
        out.println("</html>");*/
    }
}
